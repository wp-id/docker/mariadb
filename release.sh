#!/bin/sh

IMAGE_TAG="${IMAGE_NAME}:${CI_COMMIT_TAG}"

if [ ! -z "${1+x}" ]; then
    IMAGE_TAG="${IMAGE_TAG}-${1}"
fi

docker pull $IMAGE_NAME || true
docker tag $IMAGE_NAME $IMAGE_TAG
docker push $IMAGE_TAG
