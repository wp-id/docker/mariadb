# MariaDB Container
Minimalist MariaDB 1.0 container on Alpine Linux.

## Usage
See [official repo](https://hub.docker.com/_/mariadb/).
