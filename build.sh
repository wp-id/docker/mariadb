#!/bin/sh

if [ -z ${1+x} ]; then
    LATEST_IMAGE="${IMAGE_NAME}"
    DOCKERFILE=Dockerfile
else
    LATEST_IMAGE="${IMAGE_NAME}:${1}"
    DOCKERFILE="variants/${1}/Dockerfile"
fi

docker pull $LATEST_IMAGE || true
docker build --cache-from $LATEST_IMAGE -f $DOCKERFILE -t $LATEST_IMAGE .
docker push $LATEST_IMAGE
